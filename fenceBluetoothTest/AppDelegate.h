//
//  AppDelegate.h
//  fenceBluetoothTest
//
//  Created by Alex Nadein on 12/19/17.
//  Copyright © 2017 Alex Nadein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


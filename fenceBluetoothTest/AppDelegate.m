//
//  AppDelegate.m
//  fenceBluetoothTest
//
//  Created by Alex Nadein on 12/19/17.
//  Copyright © 2017 Alex Nadein. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    return YES;
}

@end

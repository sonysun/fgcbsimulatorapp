//
//  ViewController.m
//  fenceBluetoothTest
//
//  Created by Alex Nadein on 12/19/17.
//  Copyright © 2017 Alex Nadein. All rights reserved.
//

#import "ViewController.h"
#import "FGCBPeripheralServer.h"
#import <CoreBluetooth/CoreBluetooth.h>
@import QuartzCore;

static NSString *serviceUUID = @"6e400001-b5a3-f393-e0a9-e50e24dcca9e";
static NSString *txCharacteristicUUID = @"6e400002-b5a3-f393-e0a9-e50e24dcca9e";
static NSString *rxCharacteristicUUID = @"6e400003-b5a3-f393-e0a9-e50e24dcca9e";
static NSString *deviceInformationServiceUUID = @"180A";
static NSString *hardwareRevisionStringUUID = @"2A27";

@interface ViewController () <FGCBPeripheralServerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *latitudetextField;
@property (weak, nonatomic) IBOutlet UITextField *longitudeTextField;
@property (weak, nonatomic) IBOutlet UITextField *angleFrontTextField;
@property (weak, nonatomic) IBOutlet UITextField *angleRearTextField;
@property (weak, nonatomic) IBOutlet UITextField *lenghtTextField;
@property (weak, nonatomic) IBOutlet UITextField *tiltTextField;

@property (copy, nonatomic) NSString *latitude;
@property (copy, nonatomic) NSString *longitude;
@property (copy, nonatomic) NSString *angleFront;
@property (copy, nonatomic) NSString *angleRear;
@property (copy, nonatomic) NSString *lenght;
@property (copy, nonatomic) NSString *tilt;

@property(nonatomic, strong) FGCBPeripheralServer *manager;
@property(nonatomic, strong) NSTimer *sendDataTimer;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupData];
    [self setupPeripheralServer];
}


#pragma mark - IBAction


- (IBAction)tappedStart:(UIButton *)sender
{
    __weak typeof(self) weakSelf = self;
    [self setSendDataTimer:[NSTimer scheduledTimerWithTimeInterval:0.2 repeats:YES block:^(NSTimer * _Nonnull timer) {
        [weakSelf sendData];
    }]];
    [[self sendDataTimer] fire];
}

- (IBAction)tappedStop:(UIButton *)sender
{
    [[self sendDataTimer] invalidate];
}

- (IBAction)textFieldEditingDidEnd:(UITextField *)sender
{
    if ([self latitudetextField] == sender)
    {
        [self setLatitude:[sender text]];
    }
    else if ([self longitudeTextField] == sender)
    {
        [self setLongitude:[sender text]];
    }
    else if ([self angleFrontTextField] == sender)
    {
        [self setAngleFront:[sender text]];
    }
    else if ([self angleRearTextField] == sender)
    {
        [self setAngleRear:[sender text]];
    }
    else if ([self lenghtTextField] == sender)
    {
        [self setLenght:[sender text]];
    }
    else if ([self tiltTextField] == sender)
    {
        [self setTilt:[sender text]];
    }
}


#pragma mark - Setup


- (void)setupPeripheralServer
{
    [self setManager:[[FGCBPeripheralServer alloc] initWithDelegate:self]];
    CBUUID *serviceCBUUID = [CBUUID UUIDWithString:serviceUUID];
    CBUUID *characteristicCBUUID = [CBUUID UUIDWithString:rxCharacteristicUUID];

    [[self manager] setServiceName:deviceInformationServiceUUID];
    [[self manager] setServiceUUID:serviceCBUUID];
    [[self manager] setCharacteristicUUID:characteristicCBUUID];

    CBUUID *serviceSecondCBUUID = [CBUUID UUIDWithString:deviceInformationServiceUUID];
    CBUUID *characteristicSecondCBUUID = [CBUUID UUIDWithString:hardwareRevisionStringUUID];

    [[self manager] setSecondServiceUUID:serviceSecondCBUUID];
    [[self manager] setSecondCharacteristicUUID:characteristicSecondCBUUID];

    [[self manager] startAdvertising];
}

- (void)setupData
{
    [self setLatitude:@"44.01"];
    [self setLongitude:@"-121.963"];
    [self setAngleFront:@"79.541016"];
    [self setAngleRear:@"228.427754"];
    [self setLenght:@"69"];
    [self setTilt:@"60"];

    [[self latitudetextField] setText:[self latitude]];
    [[self longitudeTextField] setText:[self longitude]];
    [[self angleFrontTextField] setText:[self angleFront]];
    [[self angleRearTextField] setText:[self angleRear]];
    [[self lenghtTextField] setText:[self lenght]];
    [[self tiltTextField] setText:[self tilt]];
}

- (NSString *)documentDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

- (void)sendData
{
    [self sendValue:[NSString stringWithFormat:@"26:%@", [self getCurrentTime]] withDelay:0.01];
    [self sendValue:@"31=0" withDelay:0.02];
    [self sendValue:@"0=0" withDelay:0.03];
    [self sendValue:[NSString stringWithFormat:@"2~%@", [self lenght]] withDelay:0.04];
    [self sendValue:[NSString stringWithFormat:@"5~%@", [self tilt]] withDelay:0.05];
    [self sendValue:[NSString stringWithFormat:@"6~%@", [self angleFront]] withDelay:0.06];
    [self sendValue:[NSString stringWithFormat:@"7~%@", [self angleRear]] withDelay:0.07];
    [self sendValue:[NSString stringWithFormat:@"11~%@", [self latitude]] withDelay:0.08];
    [self sendValue:[NSString stringWithFormat:@"12~%@", [self longitude]] withDelay:0.09];
    [self sendValue:[NSString stringWithFormat:@"27:%@", [self getCurrentDate]] withDelay:0.1];
    [self sendValue:@"31=0" withDelay:0.11];
    [self sendValue:@"0=0" withDelay:0.12];
}

- (void)sendValue:(NSString *)value withDelay:(float)delay
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[self manager] sendToSubscribers:[value dataUsingEncoding:NSUTF8StringEncoding]];
    });
}

- (NSString *)getCurrentDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];

    NSDate *currentDate = [NSDate date];
    return [formatter stringFromDate:currentDate];
}

- (NSString *)getCurrentTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];

    NSDate *currentDate = [NSDate date];
    return [formatter stringFromDate:currentDate];
}


#pragma mark - FGCBPeripheralServerDelegate


- (void)peripheralServer:(FGCBPeripheralServer *)peripheral centralDidSubscribe:(CBCentral *)central
{
    NSLog(@"Central Did Subscribe");
}

- (void)peripheralServer:(FGCBPeripheralServer *)peripheral centralDidUnsubscribe:(CBCentral *)central
{
    NSLog(@"Central Did Unsubscribe");
}

@end

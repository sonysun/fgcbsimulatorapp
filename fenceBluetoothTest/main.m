//
//  main.m
//  fenceBluetoothTest
//
//  Created by Alex Nadein on 12/19/17.
//  Copyright © 2017 Alex Nadein. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

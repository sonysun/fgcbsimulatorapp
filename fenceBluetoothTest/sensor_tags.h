//
//  sensor_tags.h
//  fenceBluetoothTest
//
//  Created by mogozina on 2/8/18.
//  Copyright © 2018 Alex Nadein. All rights reserved.
//

#ifndef sensor_tags_h
#define sensor_tags_h

enum sensor_tags {
    ST_version,
    ST_multi,
    ST_length,
    ST_height_front,
    ST_height_rear,
    ST_tilt,
    ST_angle_front,
    ST_angle_rear,
    ST_rfid_front,
    ST_rfid_rear,
    ST_heading,
    ST_latitude,
    ST_longitude,
    ST_altitude,
    ST_fix_quality,
    ST_accel_x,
    ST_accel_y,
    ST_accel_z,
    ST_magnet_x,
    ST_magnet_y,
    ST_magnet_z,
    ST_gyro_x,
    ST_gyro_y,
    ST_gyro_z,
    ST_barometer,
    ST_temperature,
    ST_time,
    ST_date,
    ST_speed,
    ST_direction,
    ST_n_satellites,
    ST_num_tags,
};

#endif /* sensor_tags_h */
